# Docker compression

This project was created in the Relaxdays Code Challenge Vol. 1. See https://sites.google.com/relaxdays.de/hackathon-relaxdays/startseite for more information. My participant ID in the challenge was: CC-VOL1-30


## built the image

with `docker build -t dockerfile-compression .`

## running it

1. place the file in the projects root folder and name it `dockerfile-to-compress`
2. run `docker run -v "$(pwd)/dockerfile-to-compress:/input/dockerfile-to-compress" -v "$(pwd)/output:/output" -it dockerfile-compression` in the project root
3. the output will be in ./output/dockerfile-to-compress.compressed

### during development

- use this to mount the current folder: `docker run -v "$(pwd)/dockerfile-to-compress:/input/dockerfile-to-compress" -v "$(pwd)/output:/output" -v "$(pwd):/code" -it dockerfile-compression`
