#!/usr/bin/env python3

import os
import sys
import logging
import shutil

INPUT_FILE_UNCOMPRESSED = '/input/dockerfile-to-compress'
OUTPUT_FILE_COMPRESSED = '/output/dockerfile-to-compress.compressed'

uncompressed_image_path = '/tmp/uncompressed_image'
restic_repo_path = '/tmp/restic_repo'


def compress():
    assert os.path.isfile(INPUT_FILE_UNCOMPRESSED), 'input is not a file'
    
    try:
        sys.path.remove(OUTPUT_FILE_COMPRESSED)
    except:
        pass
    
    with open(INPUT_FILE_UNCOMPRESSED, 'r') as original:
        with open(OUTPUT_FILE_COMPRESSED, 'w') as output:
            output_lines = []
            for line in original.readlines():
                line = line.strip()
                if line == '' or line.startswith('#'):
                    continue
                    
                output_lines.append(line)
            output.write('\n'.join(output_lines))


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout)
    
    compress()
