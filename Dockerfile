FROM python:3.9

COPY . /code

ENTRYPOINT ["python3", "/code/main.py"]
